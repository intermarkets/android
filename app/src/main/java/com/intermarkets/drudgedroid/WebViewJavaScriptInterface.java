package com.intermarkets.drudgedroid;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Created by Deeeev on 08/07/2015.
 */
public class WebViewJavaScriptInterface{

    private Context context;

    /*
     * Need a reference to the context in order to sent a post message
     */
    public WebViewJavaScriptInterface(Context context){
        this.context = context;
    }


    @JavascriptInterface
    public void ReWrite(){
        MainActivity.WriteHTML(0);
    }

    @JavascriptInterface
    public void ReadyForJSON(){
        MainActivity.FireJS();
    }

    /*
     * This method can be called from Android. @JavascriptInterface
     * required after SDK version 17.
     */
    @JavascriptInterface
    public void makeToast(String message, boolean lengthLong){
        Toast.makeText(context, message, (lengthLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
    }
}