/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */



var app = {
	

	
    // Application Constructor
    initialize: function() {



        $(document).on('click', "a[href^='http://']", function (e) {

            window.open( this.href , '_system', 'location=yes' );
            this.href = '/#';

        });


        this.bindEvents();

        if(window.innerWidth < 768) {
            ShowFirst();
        }
        
        if(window.innerWidth >= 768) {
            $('.navbar').css("display", "none");

            $('#leftHandCol').css("width", "33%");
            $('#middleCol').css("width", "33%");
            $('#rightHandCol').css("width", "33%");

            $('#leftHandCol').addClass("col-sm-4");
            $('#middleCol').addClass("col-sm-4");
            $('#rightHandCol').addClass("col-sm-4");

            ShowAll();
        } else {
            $('.navbar').css("display", "block");
            $('#TopButton').addClass("active");

            $('#leftHandCol').css("width", "100%");
            $('#middleCol').css("width", "100%");
            $('#rightHandCol').css("width", "100%");

            $('#leftHandCol').removeClass("col-sm-4");
            $('#middleCol').removeClass("col-sm-4");
            $('#rightHandCol').removeClass("col-sm-4");

            ShowFirst();
        }
        
        
        $( window ).resize(function() {
                           if(window.innerWidth >= 768) {
                           $('.navbar').css("display", "none");

                           $('#leftHandCol').css("width", "33%");
                                       $('#middleCol').css("width", "33%");
                                       $('#rightHandCol').css("width", "33%");

                           $('#leftHandCol').addClass("col-sm-4");
                                       $('#middleCol').addClass("col-sm-4");
                                       $('#rightHandCol').addClass("col-sm-4");

                           ShowAll();   
                           } else {
                           $('.navbar').css("display", "block");

                           $('#leftHandCol').removeClass("col-sm-4");
                           $('#middleCol').removeClass("col-sm-4");
                           $('#rightHandCol').removeClass("col-sm-4");

                           $('#leftHandCol').css("width", "100%");
                           $('#middleCol').css("width", "100%");
                           $('#rightHandCol').css("width", "100%");


                           ShowFirst();
                           }
                           
                           });
        
        
        $("#TopButton").click(function() {
                              $('#LeftButton').removeClass("active");
                              $('#MidButton').removeClass("active");
                              $('#RightButton').removeClass("active");
                              $('#TopButton').addClass("active");
                              
                              $('html, body').animate({
                                                      scrollTop: $("#topAnch").offset().top
                                                      }, 500);
                              savedPos = "top";
                              });
        
        $("#LeftButton").click(function() {
                               $('#LeftButton').addClass("active");
                               $('#MidButton').removeClass("active");
                               $('#RightButton').removeClass("active");
                               $('#TopButton').removeClass("active");
                               
                               ShowFirst();
                               $('html, body').animate({
                                                       scrollTop: $("#leftHandCol").offset().top-50
                                                       }, 500);
                               savedPos = "left";
                               });
        
        $("#MidButton").click(function() {
                              $('#LeftButton').removeClass("active");
                              $('#MidButton').addClass("active");
                              $('#RightButton').removeClass("active");
                              $('#TopButton').removeClass("active");
                              ShowSecond();
                              $('html, body').animate({
                                                      scrollTop: $("#middleCol").offset().top-50
                                                      }, 500);
                              savedPos = "mid";
                              });
        
        $("#RightButton").click(function() {
                                $('#LeftButton').removeClass("active");
                                $('#MidButton').removeClass("active");
                                $('#RightButton').addClass("active");
                                $('#TopButton').removeClass("active");
                                ShowThird();
                                $('html, body').animate({
                                                        scrollTop: $("#rightHandCol").offset().top-50
                                                        }, 500);
                                savedPos = "right";
                                });

    },



    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
     //   document.addEventListener('deviceready', this.onDeviceReady, false);
        $(document).ready(function(){
            $(".app").hide() ;


          //  window.analytics.startTrackerWithId('UA-451855-9');
         //   window.analytics.trackView('Home Page');


           // getContent();
/*
           var cacheObject =window.localStorage.getItem("cache");



               if (cacheObject != null)
               {
               alert ("cache");
                   var cache =  JSON.parse(cacheObject);
                   dateString = cache.timestamp,
                   now = new Date().getTime().toString();

                   // cache for 10 mins currently
                   if (!compareTimeForReload(dateString, now,60000)) {
                       $("#topline").html(" ");
                       $("#topline").html(cache.headline);


                       $("#leftColContent").html(" ");
                       $("#leftColContent").append(cache.firstColumn);

                       $("#leftColContent").find("b").append(cache.firstColumnB);

                       $("#midColContent").html(" ");
                       $("#midColContent").append(cache.secondColumn);

                       $("#rightColContent").html(" ");

                       $("#rightColContent").append(cache.thirdColumn);


                       $("#rightColContent").find("b").append(cache.thirdColumnB);

                       $("#rightColContent").find("b").append(cache.thirdColumnC);

                      // alert ("Cache LAOD");
                       $("img").addClass("img-responsive");

                       $("#app_topstories img").css("width", "200px");

                       ShowHidden();

                       return;

                   }
               }
*/

           drudgeapp.ReadyForJSON();


        });
    },
    
  
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
      //  window.open = cordova.InAppBrowser.open;

       // app.receivedEvent('deviceready');
        $(".app").hide() ;
   
      //  window.analytics.startTrackerWithId('UA-451855-9');
	  //  window.analytics.trackView('Home Page');
       
        
       // getContent();
        
    
      
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
    /*    var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);*/
        
       
    }
};

function ShowFirst() {
    $('#leftHandCol').css("display", "block");
    $('#middleCol').css("display", "none");
    $('#rightHandCol').css("display", "none");
}

function ShowSecond() {
    $('#leftHandCol').css("display", "none");
    $('#middleCol').css("display", "block");
    $('#rightHandCol').css("display", "none");
}

function ShowThird() {
    $('#leftHandCol').css("display", "none");
    $('#middleCol').css("display", "none");
    $('#rightHandCol').css("display", "block");
}

function ShowAll() {
    $('#leftHandCol').css("display", "block");
    $('#middleCol').css("display", "block");
    $('#rightHandCol').css("display", "block");
}

function ShowHidden() {
    $('#topAd').css("display", "block");
    $('#tColAd').css("display", "block");
    $('#mtColAd').css("display", "block");
    $('#mbColAd').css("display", "block");
    $('#rtColAd').css("display", "block");
    $('#rbColAd').css("display", "block");

    $('#loadingimg').css("display", "none");
}

function Refresh() {
    //getContent();
   // drudgeapp.ReadyForJSON();
}

function TopAnch() {
    window.location.href = "#topAnch";
}




function compareTimeForReload (pageTime, now, cacheTime)
{
    var diffMs = now - pageTime;
    
    if ( diffMs > cacheTime){
        return true;
    }
    else {
        return false;
    }
    
}

function clearCacheAndRefresh() {
    window.localStorage.removeItem("cache");
}


function WriteHeadline(st){

   $("#headlogo").empty();
   $("#headlogo").html(st);

   $("img").css("width", "100%");
   $("img").css("height", "25%");

}

function WriteTopHeadline(st){

   $("#topline").empty();
   $("#topline").html(st);


   // var divs = $('#rewrite br');
   // divs[0].remove();
  //  divs[1].remove();
  //  divs[2].remove();
  //  divs[3].remove();
  //  divs[4].remove();
   // divs[5].remove();




}


function WriteLeftCol(st){
    $("#leftTopColContent").empty();
    $("#leftTopColContent").append(st);
}


function WriteLeftColLower(st){
    $("#leftColContent").empty();
    $("#leftColContent").append(st); // $("#leftColContent").find("b").append(st);
}

function WriteMiddleCol(st){
    $("#midTopColContent").empty();
    $("#midTopColContent").append(st);
}

function WriteMiddleColBot(st){
    $("#midColContent").empty();
    $("#midColContent").append(st);
}

function WriteRightTopCol(st){
    $("#rightTopColContent").empty();
    $("#rightTopColContent").append(st);
}

function WriteRightMidCol(st){
    $("#rightColContent").append(st);
}

function WriteRightBotCol(st){
    $("#rightColContent").empty();
    $("#rightColContent").append(st); //  $("#rightColContent").find("b").append(st);

    $("img").addClass("img-responsive");

  //  $("#app_topstories img").css("width", "200px");

    ShowHidden();
}

function RemoveTopAd(){
   // $("#3582358").remove();
}

function OnClickTest(){

    $( "a" ).click(function() {
     // alert("Test: " + this);
     //$("a").attr("href", this);
        drudgeapp.ReWrite();
    });


}